let add = $('#add')
let addContent = $('#addContent')
let search = $('#search')
let searchContent = $('#searchContent')

add.click( function(){
  add.toggleClass('rotate--45')
  addContent.removeClass('hidden')
  addContent.toggleClass('slideInLeft slideOutLeft');
})

search.click( function(){
  searchContent.removeClass('hidden')
  searchContent.toggleClass('slideInLeft slideOutLeft');
})

$('#posts .overflow-y-scroll .cursor-pointer').click(function(){
  $('#posts .overflow-y-scroll .cursor-pointer').removeClass('bg-grey-lighter');
  $(this).toggleClass('bg-grey-lighter');
  $('#edit').toggleClass('w-0 md:w-full');
  $('#posts').toggleClass('max-w-xs px-4 hidden');
  $('#posts > div:first-child').toggleClass('pr-4');
  $('#posts').find('.overflow-y-scroll .font-bold').toggleClass('whitespace-normal truncate text-lg')
  $('#post').toggleClass('flex hidden');
  $('.toggle-hide').parent().toggleClass('hidden');
  $('.toggle-margin').toggleClass('mt-2');
})

$('.go-back').click(function(){
  $('#posts .overflow-y-scroll .cursor-pointer').removeClass('bg-grey-lighter');
  $('#post').toggleClass('flex hidden');
  $('#posts').toggleClass('max-w-xs px-4');
  $('.toggle-hide').parent().toggleClass('hidden');
  $('#edit').toggleClass('w-0 md:w-full');
  $('#posts > div:first-child').toggleClass('pr-4');
  $('.toggle-margin').toggleClass('mt-2');
  $('#posts').find('.overflow-y-scroll .font-bold').toggleClass('whitespace-normal truncate text-lg')
})

$(function() {
  // Handler for .ready() called.
  $('.datepicker').pickadate({
    weekdaysShort: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa'],
    showMonthsShort: true
  })
});
