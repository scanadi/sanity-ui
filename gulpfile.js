// project paths are set in package.json
const paths = require("./package.json").paths;

const gulp = require("gulp");
const postcss = require("gulp-postcss");
const purgecss = require("gulp-purgecss");
const cleanCSS = require('gulp-clean-css');
const minify = require('gulp-minify');
const concat = require('gulp-concat');
const tailwindcss = require("tailwindcss");
const browserSync = require("browser-sync").create();

// Custom extractor for purgeCSS, to avoid stripping classes with `:` prefixes
class TailwindExtractor {
  static extract(content) {
    return content.match(/[A-z0-9-:\/]+/g) || [];
  }
}

// Copy Html and JS to dist
gulp.task('copy', function () {
    gulp.src(paths.src.base + "*.html")
      .pipe(gulp.dest(paths.dist.base));
    gulp.src(paths.src.fonts + "*.*")
      .pipe(gulp.dest(paths.dist.fonts));
    gulp.src(paths.src.images + "*.*")
      .pipe(gulp.dest(paths.dist.images));
    gulp.src(paths.src.vendors + "*.*")
      .pipe(gulp.dest(paths.dist.vendors));
});

// compiling tailwind CSS
gulp.task("css", () => {
  return gulp
    .src(paths.src.css + "*.css")
    .pipe(
      postcss([tailwindcss(paths.config.tailwind), require("autoprefixer")])
    )
    .pipe(
      purgecss({
        content: [paths.src.base + "*.html",paths.src.javascript + "*.js"],
        extractors: [
          {
            extractor: TailwindExtractor,
            extensions: ["html", "js"]
          }
        ]
      })
    )
    .pipe(cleanCSS({compatibility: 'ie11'}))
    .pipe(concat('styles.min.css'))
    .pipe(gulp.dest(paths.dist.css));
});

// compiling tailwind CSS
gulp.task("minify", () => {
  return gulp
    .src(paths.src.javascript + "*.js")
    .pipe(minify({
        ext:{
            min:'.min.js'
        },
        ignoreFiles: ['.min.js']
    }))
    .pipe(gulp.dest(paths.dist.javascript));
});

// browser-sync dev server
gulp.task("serve", ["copy","minify","css"], () => {
  browserSync.init({
    server: {
      baseDir: "./dist/"
    }
  });

  gulp.watch(paths.src.css + "*.css", ["css"]).on("change", browserSync.reload);
  gulp.watch(paths.config.tailwind, ["css"]).on("change", browserSync.reload);
  gulp.watch(paths.src.base + "*.html", ["css", "copy"]).on("change", browserSync.reload);
  gulp.watch(paths.src.images + "*.*", ["css", "copy"]).on("change", browserSync.reload);
  gulp.watch(paths.src.javascript + "*.*", ["minify"]).on("change", browserSync.reload);
});

// default task
gulp.task("default", ["serve"]);

gulp.task("build", ["copy","minify","css"])
